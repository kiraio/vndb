ALTER TABLE threads DROP COLUMN poll_preview;
ALTER TABLE threads DROP COLUMN poll_recast;
ALTER TABLE users DROP COLUMN filter_vn;
ALTER TABLE users DROP COLUMN filter_release;
ALTER TABLE users DROP COLUMN show_nsfw;
ALTER TABLE users DROP COLUMN vn_list_own;
ALTER TABLE users DROP COLUMN vn_list_wish;
ALTER TABLE vn DROP COLUMN c_olang;
