ALTER TABLE producers      ALTER COLUMN website TYPE varchar(1024);
ALTER TABLE producers_hist ALTER COLUMN website TYPE varchar(1024);
ALTER TABLE releases       ALTER COLUMN website TYPE varchar(1024);
ALTER TABLE releases_hist  ALTER COLUMN website TYPE varchar(1024);
