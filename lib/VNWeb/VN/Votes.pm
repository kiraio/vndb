package VNWeb::VN::Votes;

use VNWeb::Prelude;


sub listing_ {
    my($opt, $count, $lst) = @_;

    my sub url { '?'.query_encode %$opt, @_ }
    paginate_ \&url, $opt->{p}, [ $count, 50 ], 't';
    article_ class => 'browse votelist', sub {
        table_ class => 'stripe', sub {
            thead_ sub { tr_ sub {
                td_ class => 'tc1', sub { txt_ 'Date'; sortable_ 'date',  $opt, \&url; debug_ $lst };
                td_ class => 'tc2', sub { txt_ 'Vote'; sortable_ 'vote',  $opt, \&url; };
                td_ class => 'tc3', sub { txt_ 'User'; sortable_ 'title', $opt, \&url; };
            } };
            tr_ sub {
                td_ class => 'tc1', fmtdate $_->{date};
                td_ class => 'tc2', fmtvote $_->{vote};
                td_ class => 'tc3', sub {
                    small_ 'hidden' if $_->{c_private};
                    user_ $_ if !$_->{c_private};
                };
            } for @$lst;
        };
    };
    paginate_ \&url, $opt->{p}, [ $count, 50 ], 'b';
}


TUWF::get qr{/$RE{vid}/votes}, sub {
    my $v = dbobj tuwf->capture('id');
    return tuwf->resNotFound if !$v->{id} || $v->{entry_hidden};

    my $opt = tuwf->validate(get =>
        p => { page => 1 },
        o => { onerror => 'd', enum => ['a','d'] },
        s => { onerror => 'date', enum => ['date', 'title', 'vote' ] }
    )->data;

    my $fromwhere = sql
        'FROM ulist_vns uv
         JOIN users u ON u.id = uv.uid
        WHERE uv.vid =', \$v->{id}, 'AND uv.vote IS NOT NULL
          AND NOT EXISTS(SELECT 1 FROM users u WHERE u.id = uv.uid AND u.ign_votes)';

    my $count = tuwf->dbVali('SELECT COUNT(*)', $fromwhere);

    my $lst = tuwf->dbPagei({results => 50, page => $opt->{p}},
      'SELECT uv.vote, uv.c_private, ', sql_totime('uv.vote_date'), 'as date, ', sql_user(),
           $fromwhere, 'ORDER BY', sprintf
            { date => 'uv.vote_date %s, uv.vote', vote => 'uv.vote %s, uv.vote_date', title => "(CASE WHEN uv.c_private THEN NULL ELSE u.username END) %s, uv.vote_date" }->{$opt->{s}},
            { a => 'ASC', d => 'DESC' }->{$opt->{o}}
    );

    framework_ title => "Votes for $v->{title}[1]", dbobj => $v, sub {
        article_ sub {
            h1_ "Votes for $v->{title}[1]";
            p_ 'No votes to list. :(' if !@$lst;
        };
        listing_ $opt, $count, $lst if @$lst;
    };
};


1;
