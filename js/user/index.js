// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-3.0-only
// @source: https://code.blicky.net/yorhel/vndb/src/branch/master/js
// SPDX-License-Identifier: AGPL-3.0-only
"use strict";

@include .gen/user.js
@include user/Subscribe.js
@include user/UserLogin.js
@include user/UserEdit.js
@include user/UserAdmin.js

// @license-end
